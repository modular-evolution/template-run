// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Obstacle.generated.h"

/**
 * Object the generates overlap events with the projectile and player.
 */

//Forward declaration.
class UStaticMeshComponent;
class AProjectile;
class ARunnerPawn;
class UPrimitiveComponent;

UCLASS()
class TEMPLATERUN_API AObstacle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObstacle();

protected:
	// Mesh of the object.
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent * Mesh;

	// Called at the start of the game.
	virtual void BeginPlay() override;

	/*Called when a projectile overlaps with the mesh.*/
	UFUNCTION(BlueprintNativeEvent, Category = "Events")
	void OnProjectileOverlap(AProjectile* Projectile, FVector ImpactPoint);

	/*Called when the runner overlaps with the mesh.*/
	UFUNCTION(BlueprintNativeEvent, Category = "Events")
	void OnRunnerOverlap(ARunnerPawn* RunnerPawn);
	
	UFUNCTION()
	/*Overrides the overlap with the mesh.*/
	void MeshOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};

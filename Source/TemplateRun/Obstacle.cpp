// Fill out your copyright notice in the Description page of Project Settings.


#include "Obstacle.h"
#include "Components/StaticMeshComponent.h"
#include "Projectile.h"
#include "RunnerPawn.h"
#include "Components/PrimitiveComponent.h"

// Sets default values
AObstacle::AObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create the mesh.
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	
	// Set mesh as root to use collider for physics.
	SetRootComponent(Mesh);
}

void AObstacle::BeginPlay()
{
	Super::BeginPlay();

	// Register our function as listener for overlaps.
	Mesh->OnComponentBeginOverlap.AddDynamic(this, &AObstacle::MeshOverlap);
}

/*Overrides the overlap with the mesh.*/
void AObstacle::MeshOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Generate event for projectile events.
	AProjectile* Projectile = Cast<AProjectile>(OtherActor);
	if (Projectile != nullptr)
	{
		OnProjectileOverlap(Projectile, SweepResult.Location);
	}

	// Generate event for runner events.
	ARunnerPawn* RunnerPawn = Cast<ARunnerPawn>(OtherActor);
	if (RunnerPawn != nullptr)
	{
		OnRunnerOverlap(RunnerPawn);
	}
	
}

/*Called when a projectile overlaps with the mesh.*/
void AObstacle::OnProjectileOverlap_Implementation(AProjectile* Projectile, FVector ImpactPoint)
{
	// Additional code if needed.
}

/*Overrides the overlap with the mesh.*/
void AObstacle::OnRunnerOverlap_Implementation(ARunnerPawn* RunnerPawn)
{
	// Additional code if needed.
}


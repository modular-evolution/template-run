// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SplinePath.generated.h"

/**
* Creates a path based on a spline curve with a specific mesh.
*/

// Forward delcaration.
class USplineComponent;
class UStaticMesh;

UCLASS()
class TEMPLATERUN_API ASplinePath : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASplinePath();
	
	// Constructs the spline's mesh.
	void OnConstruction(const FTransform& Transform) override;

private:
	// Cleas the current children.
	void ClearChildrenFromSpline();

protected:
	// Holds the data about the spline curve.
	UPROPERTY(EditAnywhere, Category = Path)
		USplineComponent* SplinePath;

	// Mesh used to create the spline's mesh based on curve.
	UPROPERTY(EditAnywhere, Category = Path)
		UStaticMesh* SplineMesh;

	// Width of the mesh created based on the curve.
	UPROPERTY(EditAnywhere, Category = Path)
		float SplinePathWidth;

public:
	// Returns a location on the spline with an offset.
	UFUNCTION(BlueprintCallable, Category = Path)
	FVector GetSplineLocation(float distance, float offset);

	// Returns a rotation on the spline with an offset.
	FRotator GetSplineRotation(float distance);

};

// Fill out your copyright notice in the Description page of Project Settings.

#include "Projectile.h"


// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.
	PrimaryActorTick.bCanEverTick = true;
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Calculate the movement direction
	FVector DeltaMovement = GetActorForwardVector() * Speed * DeltaTime;

	// Move the actor with physics.
	RootComponent->MoveComponent(DeltaMovement, GetActorRotation(), false); 
}


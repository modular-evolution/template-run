// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TemplateRunGameMode.generated.h"

/**
* Holds the game rules and default classes.
*/

UCLASS(MinimalAPI)
class ATemplateRunGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATemplateRunGameMode();
};




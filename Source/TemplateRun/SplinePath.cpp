// Fill out your copyright notice in the Description page of Project Settings.

#include "SplinePath.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"

// Sets default values
ASplinePath::ASplinePath()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a spline to define the curve.
	SplinePath = CreateDefaultSubobject<USplineComponent>("Spline Path");
	
	// Set as root component.
	SetRootComponent(SplinePath);
}

// Constructs the spline's mesh.
void ASplinePath::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (SplineMesh == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Missing Mesh for the spline's mesh component on %s"), *GetOwner()->GetName())
		return;
	}

	// Clear the children to free memory.
	ClearChildrenFromSpline();

	for (int i = 0; i < SplinePath->GetNumberOfSplinePoints() - 1; i++)
	{
		// Calculate starting details.
		FVector StartLocation;
		FVector StartTangent;

		SplinePath->GetLocationAndTangentAtSplinePoint(i, StartLocation, StartTangent, ESplineCoordinateSpace::Local);

		// Calculate ending details.
		FVector EndLocation;
		FVector EndTangent;

		SplinePath->GetLocationAndTangentAtSplinePoint(i + 1, EndLocation, EndTangent, ESplineCoordinateSpace::Local);

		// Create the mesh for the spline and register component.
		USplineMeshComponent* s = NewObject<USplineMeshComponent>(this, USplineMeshComponent::StaticClass());
		s->RegisterComponent();

		// Set the mobility and physics..
		s->SetMobility(EComponentMobility::Movable);
		s->SetCollisionResponseToAllChannels(ECR_Block);
		s->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

		// Add the mesh and deform it.
		s->SetStaticMesh(SplineMesh);
		s->SetStartAndEnd(StartLocation, StartTangent, EndLocation, EndTangent, true);

		// Attach deformed mesh to the spline (root).
		s->AttachToComponent(SplinePath, FAttachmentTransformRules::KeepRelativeTransform);

		//TODO: Make mesh more wide.
	}
}


// Cleas the current children.
void ASplinePath::ClearChildrenFromSpline()
{
	// Get all current children
	auto children = SplinePath->GetAttachChildren();

	// Remove the current Children to clear memory.
	for (int i = 0; i < children.Num(); i++)
	{
		children[i]->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		children[i]->DestroyComponent();
	}
}


FVector ASplinePath::GetSplineLocation(float distance, float offset)
{
	// Find the transform at the point.
	auto DistanceTransform = SplinePath->GetTransformAtDistanceAlongSpline(distance, ESplineCoordinateSpace::World);

	// Calculate the distance along the path.
	FVector Location = DistanceTransform.GetLocation();

	// Add our offset.
	Location += DistanceTransform.GetRotation().GetRightVector() * offset;

	// Return result.
	return Location;
}

FRotator ASplinePath::GetSplineRotation(float distance)
{
	return SplinePath->GetRotationAtDistanceAlongSpline(distance, ESplineCoordinateSpace::World);
}


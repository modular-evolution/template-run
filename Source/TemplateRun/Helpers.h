// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */

class UWorld;
class AActor;

class TEMPLATERUN_API Helpers
{
public:
	typedef TSubclassOf<AActor> ActorClass;

	UFUNCTION(BlueprintCallable)
	template<class ActorType>
	static ActorType* FindObjectOfType(UWorld* world);

	Helpers();
};

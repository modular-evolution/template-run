// Fill out your copyright notice in the Description page of Project Settings.

#include "RunnerPawn.h"
#include "Components/SkeletalMeshComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "WeaponComponent.h"
#include "Helpers.h"
#include "SplinePath.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PawnMovementComponent.h"


// Sets default values
ARunnerPawn::ARunnerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create default objects and attach them.
	CapsuleCollider = CreateDefaultSubobject<UCapsuleComponent>("Capsule Collider");
	SetRootComponent(CapsuleCollider);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(CapsuleCollider);

 	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(Mesh);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	CameraComponent->SetupAttachment(CameraBoom);

// 	WeaponComponent = CreateDefaultSubobject<UWeaponComponent>(TEXT("Weapon"));
// 	WeaponComponent->SetupAttachment(Mesh, TEXT("GripPoint"));
}

// Called when the game starts or when spawned
void ARunnerPawn::BeginPlay()
{
	Super::BeginPlay();

	//TODO: Extract function.
	// Find the path to follow.
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASplinePath::StaticClass(), FoundActors);
	if (FoundActors.Num() <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Nothing found."))
	}
	else
	{
		path = Cast<ASplinePath>(FoundActors[0]);
	}

	auto comnponents = GetComponentsByClass(UWeaponComponent::StaticClass());
	if (comnponents.Num() <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Nothing found."))
	}
	else
	{
		WeaponComponent = Cast<UWeaponComponent>(comnponents[0]);
	}
}

bool ARunnerPawn::ShouldStartJump()
{
	//TODO: Create better jump.
	if (bIsJumping == true)
	{
		bIsJumping = false;
		return true;
	}

	return false;
}

void ARunnerPawn::ResetPlayer()
{
	// Reset progress along spline.
	movementAlongSpline = 0.f;
}

// Called every frame
void ARunnerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Calculate the new point reached.
	movementAlongSpline += DeltaTime * MovementSpeed;
	movementOffsetSpline += DeltaTime * movementInput * OffsetMovementSpeed;

	FVector TargetLocation = path->GetSplineLocation(movementAlongSpline, movementOffsetSpline);
	
	// Reset the height if the destination.
	TargetLocation.Z = GetActorLocation().Z;

	// Update the runner's position.
	SetActorLocation(TargetLocation);

	// Update the runner's rotation based on spline.
	FRotator TargetRotation = path->GetSplineRotation(movementAlongSpline);
	SetActorRotation(TargetRotation);
}

// Called to bind functionality to input
void ARunnerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// set up gameplay key bindings
	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &ARunnerPawn::Jump);
	InputComponent->BindAction("Shoot", EInputEvent::IE_Pressed, this, &ARunnerPawn::Shoot);
}


void ARunnerPawn::Shoot()
{
	// Delegate to weapon.
	WeaponComponent->Fire();
}

void ARunnerPawn::Jump()
{
	// Allow the player to jump only if he is on the ground.
	if (bIsJumping == false)
	{
		FVector JumpForce = FVector::UpVector * 500.f;
		CapsuleCollider->AddImpulse(JumpForce, NAME_None, true);
		bIsJumping = true;
	}
}

void ARunnerPawn::SetMovementInput(float newInput)
{
	//TODO: Delegate this to phone rotation.
	movementInput = newInput;
}

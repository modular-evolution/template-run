// Fill out your copyright notice in the Description page of Project Settings.

#include "Helpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"

template<class ActorType>
static ActorType* Helpers::FindObjectOfType(UWorld* world)
{
	TArray<ActorType*> FoundActors;
//	UGameplayStatics::GetAllActorsOfClass(world, ActorType::StaticClass(), FoundActors);

	UE_LOG(LogTemp, Warning, TEXT("%d"), FoundActors.Num())

	if (FoundActors.Num() <= 0)
	{
			return nullptr;
	}
	else
	{
		return FoundActors[0];
	}
}

Helpers::Helpers()
{
	FindObjectOfType<Helpers>(nullptr);
}
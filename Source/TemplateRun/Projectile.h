// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

/**
 * Reponsible for moving the bullet forward.
 */

UCLASS()
class TEMPLATERUN_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

	// How fast should the projectile move in the air?
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = Firing)
	float Speed = 100.f;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
};

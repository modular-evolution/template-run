// Fill out your copynotice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RunnerUI.generated.h"

/**
 * 
 */

class UButton;
class ARunnerPawn;

UCLASS()
class TEMPLATERUN_API URunnerUI : public UUserWidget
{
	GENERATED_BODY()

public:
	// Button for the fire action.
	UPROPERTY(meta = (BindWidget), BlueprintReadOnly, Category = UIInput)
		UButton* FireButton;

	// Button for the jump action.
	UPROPERTY(meta = (BindWidget), BlueprintReadOnly, Category = UIInput)
		UButton* JumpButton;
  
	// Delegator
	ARunnerPawn* runner;

	UFUNCTION(BlueprintCallable)
	void SetUp(ARunnerPawn* runner);
	
	UFUNCTION()
	void Shoot();

	UFUNCTION()
	void Jump();
};

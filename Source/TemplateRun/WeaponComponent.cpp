// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponComponent.h"
#include "Projectile.h"
#include "DrawDebugHelpers.h"

/*Fires a projectile from the "muzzle" socket.*/
void UWeaponComponent::Fire()
{
	// Projectile assigned check.
	if (Projectile == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Missing Projectile on weapon component on %s"), *GetOwner()->GetName())
		return;
	}

	// Get transform from the socket.
	FTransform SocketTransform = GetSocketTransform("Muzzle", ERelativeTransformSpace::RTS_World);

	// Spawn the projectile at the transform location and rotation.
	GetWorld()->SpawnActor<AProjectile>(Projectile, SocketTransform.GetLocation(), SocketTransform.Rotator());
}


FVector UWeaponComponent::CalculateCrossHairPosition(bool bShouldDebug)
{
	FTransform MuzzleTransform = GetSocketTransform("Muzzle", ERelativeTransformSpace::RTS_World);

	FHitResult result;

	FVector Start = MuzzleTransform.GetLocation();
	FVector End = Start + MuzzleTransform.GetRotation().GetForwardVector() * 100000.f;

	GetWorld()->LineTraceSingleByChannel(result, Start, End, ECollisionChannel::ECC_WorldStatic);
	
	if (bShouldDebug == true)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s"), *result.Location.ToString())
		DrawDebugLine(GetWorld(), Start, End, FColor::Red, true);
	}

	return result.Location;
}

void UWeaponComponent::PutCrossHairInWorld(FVector Position)
{

}

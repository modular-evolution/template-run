// Fill out your copyright notice in the Description page of Project Settings.

#include "RunnerUI.h"
#include "Button.h"
#include "RunnerPawn.h"




void URunnerUI::SetUp(ARunnerPawn* _runner)
{
	runner = _runner;
	FireButton->OnClicked.AddDynamic(this, &URunnerUI::Shoot);
	JumpButton->OnClicked.AddDynamic(this, &URunnerUI::Jump);
}

void URunnerUI::Shoot()
{
	runner->Shoot();
}

void URunnerUI::Jump()
{
	runner->Jump();
}

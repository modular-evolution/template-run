// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "RunnerPawn.generated.h"

/**
 * Responsible for firing, movement, jumping and sending data to AnimBP.
 */

// Forward delcration
class USkeletalMeshComponent;
class UCapsuleComponent;
class USpringArmComponent;
class UCameraComponent;
class UWeaponComponent;
class ASplinePath;

UCLASS()
class TEMPLATERUN_API ARunnerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ARunnerPawn();

private:
	/*How fast should the player move along the spline?*/
	UPROPERTY(Category = Movemnt, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float MovementSpeed = 100.f;

	/*How fast should the player move left-right?*/
	UPROPERTY(Category = Movemnt, EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float OffsetMovementSpeed = 100.f;
	
	/* The mesh component */	
	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* Mesh;

	/* The capsule collider */	
	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCapsuleComponent* CapsuleCollider;

	/* The camera */
	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* CameraComponent;

	/* Camera boom positioning the camera above the character */
	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/* Camera boom positioning the camera above the character */
	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UWeaponComponent* WeaponComponent;
	
	// Path to follow.
	ASplinePath* path;

	// How far did the player go along the spline.
	float movementAlongSpline = 0.f;

	// How far did the player offset from the spline.
	float movementOffsetSpline = 0.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditDefaultsOnly, Category = Firing)
	TSubclassOf<UWeaponComponent> WeaponComponentClass;

	// Is the player now jumping?
	UPROPERTY(Category = Animations, BlueprintGetter = ShouldStartJump)
	bool bIsJumping;

	// The input from the joystick.
	UPROPERTY(Category = Animations, BlueprintReadWrite)
	FVector2D joyStickInput;

	// Input from the buttons.
	float movementInput;

	UFUNCTION(BlueprintPure)
	bool ShouldStartJump();

	UFUNCTION(BlueprintCallable)
	void ResetPlayer();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	void Shoot();
	void Jump();

	void SetMovementInput(float newInput);
};
 
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SkeletalMeshComponent.h"
#include "WeaponComponent.generated.h"

/**
 * Holds the data of a weapon.
 */

class AProjectile;

UCLASS(Blueprintable)
class TEMPLATERUN_API UWeaponComponent : public USkeletalMeshComponent
{
	GENERATED_BODY()
	
protected:
	/*Projectile to spawn when the weapon fires.*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Firing)
	TSubclassOf<AProjectile> Projectile;

public:
	/*Fires a projectile from the "muzzle" socket.*/
	void Fire();

	UFUNCTION(BlueprintCallable)
	FVector CalculateCrossHairPosition(bool bShouldDebug = false);

	UFUNCTION(BlueprintCallable)
	void PutCrossHairInWorld(FVector Position);
};
